package tealistfileconverter;

import java.util.List;

import tealistfileconverter.converters.TeaListConverter;
import tealistfileconverter.converters.TeaListConverterCsv;
import tealistfileconverter.converters.TeaListConverterDsv;
import tealistfileconverter.converters.TeaListConverterXml;

/**
 * Handles parsing of options and conversion between different files 
 * containing information about tea.
 * 
 * @author Thomas Ejnefjäll
 */
public class TeaListFileConverter 
{	
	/**
	 * Construct a TeaListFileConverter.
	 */
	private TeaListFileConverter() { }
	/**
	 * Process a tea list file converter request. The provided options (String[])
	 * must contain four values: [0] name of the file we want to read from,
	 * [1] file format of the file we want to read from, [2] name of the 
	 * file we want to create and copy the tea information to, and
	 * [3] the file format of the file we want to create.  
	 * 
	 * @param options The four options.
	 * @throws Exception If there is an error while converting the file. 
	 */
	public static void processRequest(String[] options) throws Exception  {
		if(options.length != 4) {
			throw new IllegalArgumentException("Four parameters must be provided: FileFrom FormatFrom FileTo FormatTo");
		}
		convertFile(options[0], options[1], options[2], options[3]);
	}
	/**
	 * Convert a file containing tea information from one format to another.
	 *  
	 * @param fileFrom The source file containing the tea information.
	 * @param formatFrom The format of the source file.
	 * @param fileTo The destination file for the tea information.
	 * @param formatTo The format of the destination file. 
	 * @throws Exception If there is an error while converting the file. 
	 */
	private static void convertFile(String fileFrom, String formatFrom, String fileTo, String formatTo) throws Exception {
		
		if(!FileFormat.isValid(formatFrom) || !FileFormat.isValid(formatTo)) {
			throw new IllegalArgumentException("Format not supported. Valid formats: " + FileFormat.validFormats());
		}		
		TeaListConverter tlc = null;
		List<Tea> teaList = null;
		
		if(FileFormat.CSV.equals(formatFrom)) {
			tlc = new TeaListConverterCsv();
		}
		else if(FileFormat.DSV.equals(formatFrom)) {
			tlc = new TeaListConverterDsv();
		}
		else if(FileFormat.XML.equals(formatFrom)) {
			tlc = new TeaListConverterXml();
		}
		teaList = tlc.convert(fileFrom);
		
		if(FileFormat.CSV.equals(formatTo)) {
			tlc = new TeaListConverterCsv();
		}
		else if(FileFormat.DSV.equals(formatTo)) {
			tlc = new TeaListConverterDsv();
		}
		else if(FileFormat.XML.equals(formatTo)) {
			tlc = new TeaListConverterXml();
		}
		tlc.convert(teaList, fileTo);
	}	
}